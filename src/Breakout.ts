import { TitleScreenState } from "./state_titlescreen";
import { GameState } from "./state_game";

export class Breakout {

    // Variables

    game: Phaser.Game;
        
    // Functions

    constructor() {

        // Initialize a new game
        this.game = new Phaser.Game(1024, 700, Phaser.AUTO, 'Content',
            { create: this.create, preload: this.preload });
    }

    preload() {

        // Load sprites
        this.game.load.image("BrickBlue", "Assets/Sprites/Bricks/Blue.png");
        this.game.load.image("BrickGreen", "Assets/Sprites/Bricks/Green.png");
        this.game.load.image("BrickViolet", "Assets/Sprites/Bricks/Violet.png");
        this.game.load.image("BrickYellow", "Assets/Sprites/Bricks/Yellow.png");

        this.game.load.image("TitleImage", "Assets/Sprites/TitleImage.png");
        this.game.load.image("StartButton", "Assets/Sprites/StartButton.png");
        this.game.load.image("Background", "Assets/Sprites/Background.png");
        this.game.load.image("Ball", "Assets/Sprites/Ball.png");
        this.game.load.image("Paddle", "Assets/Sprites/Paddle.png");

            // Load sound effects
        this.game.load.audio("BallHitBrick", ["Assets/Sounds/Effects/BallHitBrick/BallHitBrick.mp3",
            "Assets/Sounds/Effects/BallHitBrick/BallHitBrick.ogg",
            "Assets/Sounds/Effects/BallHitBrick/BallHitBrick.m4a"])
        this.game.load.audio("BallHitPaddle", ["Assets/Sounds/Effects/BallHitPaddle/BallHitPaddle.mp3",
            "Assets/Sounds/Effects/BallHitPaddle/BallHitPaddle.ogg",
            "Assets/Sounds/Effects/BallHitPaddle/BallHitPaddle.m4a"])
        this.game.load.audio("BallHitWall", ["Assets/Sounds/Effects/BallHitWall/BallHitWall.mp3",
            "Assets/Sounds/Effects/BallHitWall/BallHitWall.ogg",
            "Assets/Sounds/Effects/BallHitWall/BallHitWall.m4a"])
        this.game.load.audio("GameOver", ["Assets/Sounds/Effects/GameOver/GameOver.mp3",
            "Assets/Sounds/Effects/GameOver/GameOver.ogg",
            "Assets/Sounds/Effects/GameOver/GameOver.m4a"])
        this.game.load.audio("LevelComplete", ["Assets/Sounds/Effects/LevelComplete/LevelComplete.mp3",
            "Assets/Sounds/Effects/LevelComplete/LevelComplete.ogg",
            "Assets/Sounds/Effects/LevelComplete/LevelComplete.m4a"])

        // Load music
        this.game.load.audio("BGM", ["Assets/Sounds/Music/BackgroundMusic/BackgroundMusic.mp3",
            "Assets/Sounds/Music/BackgroundMusic/BackgroundMusic.ogg",
            "Assets/Sounds/Music/BackgroundMusic/BackgroundMusic.m4a"])
        this.game.load.audio("TitleMusic", ["Assets/Sounds/Music/TitleMusic/TitleMusic.mp3",
            "Assets/Sounds/Music/TitleMusic/TitleMusic.ogg",
            "Assets/Sounds/Music/TitleMusic/TitleMusic.m4a"])
    }

    create() {

        // Add all states to the game, setting the title screen state to auto-start
        this.game.state.add("TitleScreenState", TitleScreenState, true);
        this.game.state.add("GamePlayingState", GameState, false);

        // Set the scale mode to make sure the game scales automatically
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    }
}