export class Ball extends Phaser.Sprite {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number) {

        super(game, x, y, "Ball");
    }

}