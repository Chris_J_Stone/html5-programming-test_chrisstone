export enum BrickType { BLUE, GREEN, VIOLET, YELLOW, NONE }

export class Brick extends Phaser.Sprite {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number, key: string) {

        super(game, x, y, key);
    }

}