import { Brick } from "./brick"

/* I imagine the blue brick would be the standard brick, with
 one health, and no special effects. Would heavily populate 
 early levels before more mechanics are introduced*/

export class BlueBrick extends Brick {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number, key: string) {

        super(game, x, y, key);
    }

}