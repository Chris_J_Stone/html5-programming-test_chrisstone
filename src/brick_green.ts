import { Brick } from "./brick"

/* The green brick could be another standard brick, but have 
 * more health and give more points. Would need another spirte
 * to indicate damage */

export class GreenBrick extends Brick {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number, key: string) {

        super(game, x, y, key);
    }

}