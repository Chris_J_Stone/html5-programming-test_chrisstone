import { Brick } from "./brick"

/* I want to have the violet brick explode upon being hit, damaging all
 the bricks around it. Could have a short fuse, or multiple hit points. */

export class VioletBrick extends Brick {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number, key: string) {

        super(game, x, y, key);
    }

}