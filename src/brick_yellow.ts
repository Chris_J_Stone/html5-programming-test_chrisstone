import { Brick } from "./brick"

/* When this brick takes damage, it could fill empy spaces randomly
 with other bricks, or temporarily increase the speed of the ball
 by a significant amount. This would stack with other yellow bricks*/

export class YellowBrick extends Brick {

    // Variables

    game: Phaser.Game;

    // Functions

    constructor(game: Phaser.Game, x: number, y: number, key: string) {

        super(game, x, y, key);
    }

}