import { Math, Sprite } from "phaser";

export class Paddle extends Sprite {

    // Variables

    game: Phaser.Game;
    sprite: Phaser.Sprite;
    yCoord: number;
    minX: number;
    maxX: number;

    // Functions

    constructor(game: Phaser.Game) {

        super(game, 0, 0, "Paddle");

        // We are going to have the paddle locked to this position on the Y axis
        this.yCoord = this.game.height - 100;

        // Going to clamp paddle position between these values, based on sprite width
        this.minX = this.width / 2;
        this.maxX = game.width - (this.width / 2);

        // Set the anchor to center, and set to initial position
        this.anchor.set(0.5, 0.5);
        this.position.set(game.width / 2, this.yCoord);
    }

    update() {

        //Use mouse coordinates to update the paddle position
        this.movePaddle(this.game.input.mousePointer.x);
    }

    movePaddle(newX: number) {

        //Clamp the x to within our range
        var xCoord = Math.clamp(newX, this.minX, this.maxX);
        this.position.set(xCoord, this.yCoord);
    }

}