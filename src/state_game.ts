import { Paddle } from "./paddle";
import { Ball } from "./ball";
import { BlueBrick } from "./brick_blue";
import { GreenBrick } from "./brick_green";
import { VioletBrick } from "./brick_violet";
import { YellowBrick } from "./brick_yellow";
import { BrickType } from "./brick";


export class GameState extends Phaser.State {

    // Variables

    game: Phaser.Game;
    backgroundSprite: Phaser.Sprite;
    gameMusic: Phaser.Sound;
    startText: Phaser.Text;
    levelText: Phaser.Text;
    scoreText: Phaser.Text;
    score: number
    levelNumber: number;
    space: Phaser.Key;
    brickGroup: Phaser.Group;
    playingGame: boolean;

    //paddle: Paddle
    //ball: Ball
    paddle: Phaser.Sprite;
    paddleY: number;
    ball: Phaser.Sprite;
    ballSpeed: number;

    // Defining level multidimensional arrays
    private static levelOne: BrickType[][] =
        [[BrickType.BLUE, BrickType.BLUE, BrickType.NONE, BrickType.BLUE, BrickType.BLUE],
            [BrickType.GREEN, BrickType.GREEN, BrickType.GREEN]];

    private static levelTwo: BrickType[][] =
        [[BrickType.GREEN],
            [BrickType.BLUE, BrickType.BLUE, BrickType.BLUE],
            [BrickType.BLUE, BrickType.BLUE, BrickType.BLUE, BrickType.VIOLET, BrickType.VIOLET, BrickType.BLUE, BrickType.BLUE, BrickType.BLUE],
            [BrickType.BLUE, BrickType.BLUE, BrickType.BLUE],
            [BrickType.GREEN]];

    private static levelThree: BrickType[][] =
        [[BrickType.GREEN, BrickType.BLUE, BrickType.GREEN],
            [BrickType.GREEN, BrickType.VIOLET, BrickType.GREEN],
            [BrickType.VIOLET, BrickType.YELLOW, BrickType.VIOLET],
            [BrickType.GREEN, BrickType.VIOLET, BrickType.GREEN],
            [BrickType.GREEN, BrickType.BLUE, BrickType.GREEN]];

    public static MAX_LEVEL: number = 3;

    // Functions

    constructor() {
        super();

    }

    create() {

        // Set variable's initial values
        this.score = 0;
        this.levelNumber = 1;
        this.space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        // Start the ARCADE physics engine
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // Disable gravity
        this.game.physics.arcade.gravity.y = 0;

        // Spawn all the objects in the scene
        this.spawnGameObjects();
        this.spawnGameUI();
        this.spawnLevel(this.levelNumber);
    }

    update() {

        // Update the paddle location based on mouse coordinate
        this.movePaddle(this.game.input.activePointer.x);

        if (this.playingGame == true) {

            this.checkCollisions();
            this.checkForLoss();

            // Check for win
            if (this.brickGroup.total == 0) {

                this.playingGame = false;

                // Go to the next level, after a 1 second delay
                this.game.time.events.add(Phaser.Timer.SECOND, this.incrementLevel, this);

            }
        }
        else {

            // Make ball follow paddle
            this.ball.position.set(this.paddle.position.x, this.paddle.position.y - 40);

        }
    }

    checkForLoss() {

        if (this.ball.y > this.paddle.y) {

            // If it goes past the paddle, let if fall out the world
            this.ball.body.collideWorldBounds = false;

            this.game.time.events.add(Phaser.Timer.SECOND, () => {

                // Add game over text to the screen
                var gameOverText = this.game.add.text(0, 0, "GAME OVER", {
                    font: "70px Arial",
                    fill: "#FF0000",
                    align: "center"
                });

                gameOverText.anchor.set(0.5, 0.5);
                gameOverText.position.set(this.game.width / 2, this.game.height / 2);
            });

            // After a short delay, return to the main menu
            this.game.time.events.add(Phaser.Timer.SECOND * 3, () => { this.game.state.start("TitleScreenState", true, false) });
        }
    }

    movePaddle(newX: number) {

        /* Here I would like to have used the clamp function provided in the 
         math library. However, it seems that trying to access the math library
         leaves me with a black screen. I haven't been able to find a solution,
         so I have implemented my own clamp*/

        // Clamp the parameter based on sprite width so it cannot go out of bounds
        if (newX < this.paddle.width / 2) {
            newX = this.paddle.width / 2;
        }
        else if (newX > this.game.width - (this.paddle.width / 2)) {
            newX = this.game.width - (this.paddle.width / 2);
        }

        /* I could potentially interp the x value so that the paddle doesn't move
         instantly, and the speed of the interp could be tied to difficulty? */

        // Set the position with the new clamped value
        this.paddle.position.set(newX, this.paddleY);
    }

    checkCollisions() {

        // Between ball and paddle
        this.game.physics.arcade.collide(this.ball, this.paddle, (object1: any, object2: any) => {

            // Find the distance between the ball and the paddle
            var distance = this.ball.x - this.paddle.x;

            // Give the ball extra velocity based on 
            var velocityBonus = distance * 2;
            this.ball.body.velocity.x = velocityBonus;

            // Adjust velocity
            this.normalizeBallVelocity();
        });

        // Between ball and brick
        this.game.physics.arcade.collide(this.ball, this.brickGroup, (object1: any, object2: any) => {

            // Simply destroy the brick
            (<Phaser.Sprite>object2).kill();
        });
    }

    normalizeBallVelocity() {

        // Find magnitude
        var x = this.ball.body.velocity.x
        var y = this.ball.body.velocity.y
        var length = Math.sqrt((x * x) + (y * y));

        // Normalize
        x /= length;
        y /= length;

        // Multiply by ball speed
        x *= this.ballSpeed;
        y *= this.ballSpeed;

        // Set the new velocity
        this.ball.body.velocity.x = x;
        this.ball.body.velocity.y = y;
    }

    spawnLevel(levelNumber: number) {

        /* Here we would ideally load level information from a file in the 
        * levels folder, such as brick arrangement, background image, music,
        * etc. For this example, I'm simply going to hard code a few brick
        * arrangements, and use the same background image and music.*/

        // Add the background as a sprite
        this.backgroundSprite = this.add.sprite(0, 0, "Background");

        // Set the anchor and position to center of screen
        this.backgroundSprite.anchor.set(0.5, 0.5);
        this.backgroundSprite.position.set(this.game.width / 2, this.game.height / 2);

        // Make sure the background is behind everything
        this.backgroundSprite.sendToBack();

        // Pick level arrangement based on level number
        switch (levelNumber) {
            case 1:
                {
                    this.spawnBricks(GameState.levelOne);
                    break;
                }
            case 2:
                {
                    this.spawnBricks(GameState.levelTwo);
                    break;
                }
            case 3:
                {
                    this.spawnBricks(GameState.levelThree);
                    break;
                }
        }

        // Add and play the background music, looping at full volume
        this.gameMusic = this.add.audio("BackgroundMusic");
        //this.gameMusic.play('', 0, 10, true);

        this.spawnStartGameText();

        // Create key binding for the space bar
        this.space.onDown.addOnce(this.onSpacePressed, this);

        // Decide ball speed based on level number
        this.ballSpeed = 300 + (100 * levelNumber);
    }

    spawnBricks(arrangement: BrickType[][]) {

        this.brickGroup = this.game.add.group();

        var brickSpaceX = 85;
        var brickSpaceY = 40;
        var x: number;
        var y = 100;

        var i: number;
        var j: number;

        for (i = 0; i < arrangement.length; i++) {

            // use the number of bricks in the row to find the starting position, effectively centering the row
            x = (this.game.width / 2) - ((arrangement[i].length * brickSpaceX) / 2);

            for (j = 0; j < arrangement[i].length; j++) {
                
                // Decide sprite based on brick type
                switch (arrangement[i][j]) {
                    case BrickType.BLUE:
                        {
                            this.spawnBrick("BrickBlue", x, y);
                            break;
                        }
                    case BrickType.GREEN:
                        {
                            this.spawnBrick("BrickGreen", x, y);
                            break;
                        }
                    case BrickType.VIOLET:
                        {
                            this.spawnBrick("BrickViolet", x, y);
                            break;
                        }
                    case BrickType.YELLOW:
                        {
                            this.spawnBrick("BrickYellow", x, y);
                            break;
                        }
                    case BrickType.NONE:
                        {
                            // Do nothing, we want to skip this space
                            break;
                        }
                }
                // Move spawn point along
                x += brickSpaceX;
            }
            // Move spawn point down
            y += brickSpaceY;
        }
    }

    spawnBrick(key: string, x: number, y: number) {

        /* Here I would ideally spawn a different class based on the type
         * of brick that I'm trying to make. I have my BlueBrick, GreenBrick,
         * etc. classes that extend Brick, but since Brick extends sprite
         * and that seems to be giving me issues, I have just spawned a
         * sprite, seeing as I am trying to stick to the timeframe. */

        // Add the sprite to the game
        var brick = this.game.add.sprite(x, y, key);

        // Enable physics on brick
        this.game.physics.enable(brick, Phaser.Physics.ARCADE);
        brick.body.immovable = true;

        // Bind on killed event and add to group
        brick.events.onKilled.add(this.onBrickDeath, this);
        this.brickGroup.add(brick);
    }

    onBrickDeath() {

        /* In the brick class I would have had a variable for points, which
         * I would get here to add to the score so that each brick type
         * could give different scores. Here I'll just give 100 points. */

        // Play particle effect
        
        this.addScore(100);

    }

    spawnGameObjects() {

        // Spawn paddle
        this.paddle = this.game.add.sprite(0, 0, "Paddle");
        this.paddle.anchor.set(0.5, 0.5);
        this.paddleY = this.game.height - 100;
        this.paddle.position.set(this.game.width / 2, this.paddleY);

        // Enable physics
        this.game.physics.enable(this.paddle, Phaser.Physics.ARCADE);

        // Make sure collision happens from all sides
        this.paddle.body.checkCollision.up = true;
        this.paddle.body.checkCollision.down = true;
        this.paddle.body.checkCollision.left = true;
        this.paddle.body.checkCollision.right = true;

        // Make sure paddle can't be affected by the ball
        this.paddle.body.immovable = true;

        // Spawn ball
        this.ball = this.game.add.sprite(0, 0, "Ball");
        this.ball.anchor.set(0.5, 0.5);
        this.ball.position.set(this.game.width / 2, this.game.height / 2);

        /* In this situation, we would usually want to have custom collision for the 
         * ball, so that it behaves more like a sphere. However, in this example,
         * every surface that the ball will be colliding with will be fully horizontal, 
         * or fully vertical, meaning that it will behave correctly even with box
         * collision, so I don't need to change it here.*/

        this.game.physics.enable(this.ball, Phaser.Physics.ARCADE);

        this.ball.body.checkCollision.up = true;
        this.ball.body.checkCollision.down = true;
        this.ball.body.checkCollision.left = true;
        this.ball.body.checkCollision.right = true;

        // Give the ball the ability to bounce and collide with bounds
        this.ball.body.collideWorldBounds = true;
        this.ball.body.bounce.y = this.ball.body.bounce.x = 1;        
    }

    spawnGameUI() {
        
        // Add the score text 
        this.scoreText = this.game.add.text(20, 10, "Score: " + this.score.toString(), {
            font: "25px Arial",
            fill: "#000000",
            align: "center"
        });

        // Add the level text 
        this.levelText = this.game.add.text(0, 0, "Level: " + this.levelNumber.toString(), {
            font: "25px Arial",
            fill: "#000000",
            align: "center"
        });
        
        // Anchor the level text to the middle and center it on screen
        this.levelText.anchor.set(0.5, 0);
        this.levelText.position.set(this.game.width / 2, 10);
    }

    spawnStartGameText() {

        // Add the start game text
        this.startText = this.game.add.text(0, 0, "Press SPACE to start", {
            font: "40px Arial",
            fill: "#000000",
            align: "center"
        });

        // set the anchor and the position of the start game text
        this.startText.anchor.set(0.5, 0.5);
        this.startText.position.set(this.game.width / 2, this.game.height - 200);
    }

    addScore(points: number) {

        // Add points to the total score
        this.score += points;

        // Update the UI
        this.scoreText.setText("Score: " + this.score.toString());
    }

    incrementLevel() {

        // Shut down this level
        this.brickGroup.destroy();
        this.gameMusic.stop();

        if (this.levelNumber < GameState.MAX_LEVEL) {

            // If there are more levels to come, load the next one
            this.levelNumber++;
            this.levelText.setText("Level: " + (this.levelNumber).toString());
            this.spawnLevel(this.levelNumber);

            // Reset the ball to the middle, setting it's velocity to 0
            this.ball.body.velocity.x = this.ball.body.velocity.y = 0;
            this.ball.position.set(this.game.width / 2, this.game.height / 2);
        }
        else {

            // If there are no more levels, go back to the title screen
            this.game.state.start("TitleScreenState", true, false);
        }
    }

    onSpacePressed() {

        // Remove start game text
        this.startText.destroy();

        // Launch the ball
        this.ball.body.velocity.y = -this.ballSpeed;
        this.playingGame = true;
    }
}