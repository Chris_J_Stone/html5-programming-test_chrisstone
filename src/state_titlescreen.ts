export class TitleScreenState extends Phaser.State {

    // Variables

    game: Phaser.Game;
    titleSprite: Phaser.Sprite;
    backgroundSprite: Phaser.Sprite;
    bgm: Phaser.Sound;
    playButton: Phaser.Button;

    // Functions

    constructor() {
        super();
    }

    create() {

        // Add the background as a sprite
        this.backgroundSprite = this.add.sprite(0, 0, "Background");

        // Set the anchor and position to center of screen
        this.backgroundSprite.anchor.set(0.5, 0.5);
        this.backgroundSprite.position.set(this.game.width / 2, this.game.height / 2);

        // Add the title image as a sprite 
        this.titleSprite = this.add.sprite(0, 0, "TitleImage");

        // Set the anchor to the middle of the sprite and position it in the center of the screen
        this.titleSprite.anchor.set(0.5, 0);
        this.titleSprite.position.set(this.game.width / 2, 100);

        // Add Button to the screen, setting the on callback function to our function
        this.playButton = this.game.add.button(0, 0, "StartButton", () => {

            // Stop music
            this.bgm.stop();

            // Go to the game state
            this.game.state.start("GamePlayingState", true, false);
        });

        // Set anchor and position to center of the screen
        this.playButton.anchor.set(0.5, 0.5);
        this.playButton.position.set(this.game.width / 2, this.game.height - 200);

        // Add and play the background music, looping at full volume
        this.bgm = this.add.audio("TitleMusic");
        //this.bgm.play('', 0, 10, true);
    }

    onStartButtonClicked() {

        this.bgm.stop();

        // Go to the game state
        this.game.state.start("GamePlayingState", true, false);
    }
}